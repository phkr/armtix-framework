#!/bin/tclsh
#TODO: error handling
package require unix_sockets
package require mysqltcl
package require sqlite3

# hard-coded options
set sock_path "/run/armtix/control.sock"
set poweroff 0
array set argsdef [list \
	-host [list mysql_host {MySQL host address} "host"] \
	-port [list mysql_port {MySQL port} "port"] \
	-unix [list mysql_sock {MySQL socket path} "path"] \
	-user [list mysql_user {MySQL user name} "name"] \
	-password [list mysql_password {MySQL user password} "password"] \
	-db [list mysql_db {MySQL database} "database"] \
	-compress [list mysql_compress {Compress data on MySQL connection} "compress"] \
	-sqlite [list sqlite_path {SQLite database path} "path"] \
	-socket [list socket_path {Socket to listen on} "path"]
]

proc mysql_list {arglist} {
	# list mysql database entries by filters
	global mysql_conn
	if {[lindex $arglist 0] == "table" && [lindex $arglist 1] != ""} {set table [lindex $arglist 1]} else {
		return [format {
<h1>WRONG COMMAND!</h1>
<p><a href="http://%s:%s/%s">Return to main window</a></p>
} $env(SERVER_NAME) $env(SERVER_PORT) $env(SCRIPT_NAME)]
	}

	switch -exact -- $table {
		nobuild {
			set lst [mysql::sel $mysql_conn {SELECT * FROM nobuild} -list]
			set output [format {
<h1>Nobuild list</h1>
<p>This is a list of packages which will not be built</p>
<p><a href="http://%s:%s/%s">Return to main window</a></p>
<table><caption>Nobuild list:</caption>
} $env(SERVER_NAME) $env(SERVER_PORT) $env(SCRIPT_NAME)]
			foreach {nobuild} $lst {
set output [format {
%s
<tr><td>%s</td></tr>
} $output $nobuild]
			}
			set output [format {
%s
</table>
} $output]
		}

		gitdirs {
			set lst [mysql::sel $mysql_conn {SELECT name,arm FROM gitdirs} -list]
			set output [format {
<h1>Gitdirs list</h1>
<p>This is a list of git directories</p>
<p><a href="http://%s:%s/%s">Return to main window</a></p>
<table><caption>Git directories:</caption>
<tr><th>name</th><th>patched for</th></tr>
} $env(SERVER_NAME) $env(SERVER_PORT) $env(SCRIPT_NAME)]
			foreach {dir} $lst {
				set output [format {
%s
<tr><td>%s</td><td>%s</td></tr>
} $output [lindex $dir 0] [lindex $dir 1]]
			}
			set output [format {
%s
</table>
} $output]
		}

		chains {
			set pkg [lindex $arglist 2]
			set result [lindex $arglist 3]
			if {$pkg != "" && $result != ""} {
				mysql::exec $mysql_conn [format {UPDATE packages SET status=%s WHERE pkgname='%s';} $result $pkg]
				mysql::exec $mysql_conn [format {UPDATE chains SET status=%s WHERE pkgname='%s';} $result $pkg]
			}
			set lst [mysql::sel $mysql_conn {SELECT chain,pkgname,status,source,arm,armsource FROM chains ORDER BY chain} -list]
			set output [format {
<h1>Chains list</h1>
<p>This is a list of chains of packages; these packages are not built automatically but their status is tracked</p>
<p><a href="http://%s:%s/%s">Return to main window</a></p>
<table><caption>Chains:</caption>
<tr><th>name</th><th>package</th><th>status</th><th>source</th><th>patched for</th><th>pathced source</th></tr>
} $env(SERVER_NAME) $env(SERVER_PORT) $env(SCRIPT_NAME)]
			foreach {elem} $lst {
				switch -exact -- [lindex $elem 2] {
					0 {set result OK}
					1 {set result failed}
					2 {set result available}
					3 {set result running}
					4 {set result {waits deps}}
					5 {set result {waits patch}}
					6 {set result {waits mainline}}
					7 {set result {waits build in chain}}
					8 {set result {update status processing}}
					default {set result wrong}
				}
				set output [format {
%s
<tr><td>%s</td><td>%s</td>
<td>%s <form action="%s" method="post">
<input type="hidden" name="db" value="mysql">
<input type="hidden" name="action" value="show">
<input type="hidden" name="table" value="chains">
<input type="hidden" name="package" value="%s">
<select name="result">
	<option value="0">OK</option>
	<option value="1">failed</option>
	<option value="2">available</option>
	<option value="3">running</option>
	<option value="4">waits deps</option>
	<option value="5">waits patch</option>
	<option value="6">waits mainline</option>
	<option value="7">waits build in chains</option>
	<option value="8">processing update</option>
</select>
<input type="submit" value="Change">
</form></td>

<td>%s</td><td>%s</td><td>%s</td></tr>
} $output [lindex $elem 0] [lindex $elem 1] $result $env(SCRIPT_NAME) [lindex $elem 1] [lindex $elem 3] [lindex $elem 4] [lindex $elem 5]]
			}
			set output [format {
%s
</table>
} $output]
		}

		packages {
			#TODO: at first show filters, if they are provided, show corresponding packages
		}

		default {
			set output [format {
<h1>WRONG COMMAND!</h1>
<p><a href="http://%s:%s/%s">Return to main window</a></p>
} $env(SERVER_NAME) $env(SERVER_PORT) $env(SCRIPT_NAME)]
		}
	}
	return $output
}

proc mysql_set_status {arglist} {
	# set status of chosen package
}

proc sqlite_kill {arglist} {
	# kill chosen task
}

proc sqlite_list {arglist} {
	# list sqlite database by filters
	global sqlite_db
	set values [list]
	sqlite_db eval {} value {
		set names [lrange [array names values] 0 end-1]
		set tmplist [list]
		foreach {name} $names {lappend tmplist $value($name)}
		lappend values $tmplist
	}
}

proc sqlite_toggle_agent {arglist} {
	# change agent state
}

proc operation {conn} {
	# main working function
	set cmd [gets $conn]
	set database [lindex $cmd 0]
	switch -exact $database {
		poweroff {
			global poweroff
			set poweroff 1
			return
		}
		sqlite {
			set action [lindex $cmd 1]
			switch -exact $action {
				set out_field [format {
<h1>SQLite database</h1>
<p><a href="http://%s:%s/%s">Return to main window</a></p>
#form to display state

#form to manage tasks

#form to manage agents
} $env(SERVER_NAME) $env(SERVER_PORT) $env(SCRIPT_NAME)]
				show {
					puts $conn [sqlite_list [lrange $cmd 2 end]]
				}
				kill {
					puts $conn [sqlite_kill [lrange $cmd 2 end]]
				}
				toggle {
					puts $conn [sqlite_toggle_agent [lrange $cmd 2 end]]
				}
				default {
					puts $conn 
				}
			}
		}
		mysql {
			set action [lindex $cmd 1]
			switch -exact $action {
				show {
					puts $conn [mysql_list [lrange $cmd 2 end]]
				}
				status {
					puts $conn [mysql_set_status [lrange $cmd 2 end]]
				}
				default {
					# index for mysql
					puts $conn [format {
<h1>MySQL database</h1>
<p><a href="http://%s:%s/%s">Return to main window</a></p>

<form action="%s" method="post">
<fieldset>
<legend>Nobuilds list</p>
<input type="hidden" name="db" value="mysql">
<input type="hidden" name="action" value="show">
<input type="hidden" name="table" value="nobuild">
<p><input type="submit" value="Show"></p>
</fieldset>
</form>

<form action="%s" method="post">
<fieldset>
<legend>Gitgirs list</p>
<input type="hidden" name="db" value="mysql">
<input type="hidden" name="action" value="show">
<input type="hidden" name="table" value="gitdirs">
<p><input type="submit" value="Show"></p>
</fieldset>
</form>

<form action="%s" method="post">
<fieldset>
<legend>Chains</p>
<input type="hidden" name="db" value="mysql">
<input type="hidden" name="action" value="show">
<input type="hidden" name="table" value="chains">
<p><input type="submit" value="Show"></p>
</fieldset>
</form>

<form action="%s" method="post">
<fieldset>
<legend>Chains</p>
<input type="hidden" name="db" value="mysql">
<input type="hidden" name="action" value="show">
<input type="hidden" name="table" value="packages">
<p><input type="submit" value="Show"></p>
</fieldset>
</form>
} $env(SERVER_NAME) $env(SERVER_PORT) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME)]
				}
			}
		}
		default {
			# index
			puts $conn [format {
<h1>ARMtix Mesos framework control interface</h1>
<p>This interface is used to control ARMtix Mesos framework with MySQL and SQLite databases it uses</p>
<p>By default any of the following links only show control interfaces for databases. For any actual data you should provide filters.</p>
<form action="%s" method="post">
<fieldset>
<legend>MySQL database</p>
<input type="hidden" name="db" value="mysql">
<input type="hidden" name="action" value="index">
<p><input type="submit" value="Show"></p>
</fieldset>
</form>
<form action="%s" method="post">
<fieldset>
<legend>SQLite database</p>
<input type="hidden" name="db" value="sqlite">
<input type="hidden" name="action" value="index">
<p><input type="submit" value="Show"></p>
</fieldset>
</form>
} $env(SCRIPT_NAME) $env(SCRIPT_NAME)]
		}
	}
}

proc accept {conn} {
	# accept connection on socket
	chan event $conn readable [list operation $conn]
}

# parse cmdline options
set default_opt "usage: $argv0 "
set descriptions ""
set switch_body ""

foreach {elem} [array names argsdef] {
	set switch_body [string cat $switch_body [list $elem [format {
		incr argnum
		set %s [lindex $argv $argnum]
		if {[info exists %s]} {
			if {[string length $%s]==0 || [string range $%s 0 0]=="-"} $default_opt
		}
		array unset argsdef %s
	} [lindex $argsdef($elem) 0] [lindex $argsdef($elem) 0] [lindex $argsdef($elem) 0] [lindex $argsdef($elem) 0] $elem]
	] "\n"
	]

	set descriptions [string cat $descriptions [join [list $elem "-" [lindex $argsdef($elem) 1]]] "\n"]
	set default_opt [string cat $default_opt $elem " " [lindex $argsdef($elem) 2] " "]
}

set default_opt [string cat [list puts stdout [string cat $default_opt "\n" $descriptions]] "\nexit 1"]
set switch_body [string cat $switch_body "default " [string cat "{\n" $default_opt "\n}\n"]]

if {$argc == 0} $default_opt

set argnum 0
while {$argnum < $argc} {
	set option [lindex $argv $argnum]
	switch -exact -- $option \
		$switch_body
	incr argnum
}

# set up connections
if [info exists mysql_sock] {
	if [info exists mysql_compress] {
		set compress_opt [list -compress $mysql_compress]
	} else {
		set compress_opt ""
	}
	set mysql_conn [mysql::connect -socket $mysql_sock -user $mysql_user -password $mysql_password -db $mysql_db $compress_opt]
	unset compress_opt
} elseif [info exists mysql_host] {
	if [info exists mysql_compress] {
		set compress_opt [list -compress $mysql_compress]
	} else {
		set compress_opt ""
	}
	if [info exists mysql_port] {
		set port_opt [list -port $mysql_port]
	} else {
		set port_opt ""
	}
	set mysql_conn [mysql::connect -host $mysql_host $port_opt -user $mysql_user -password $mysql_password -db $mysql_db $compress_opt]
	unset port_opt compress_opt
} else $default_opt

if [info exists sqlite_path] {
	sqlite3 sqlite_db $sqlite_path
} else $default_opt

set listen [unix_sockets::listen $sock_path accept]
vwait poweroff

mysql::close $mysql_conn
sqlite_db close
