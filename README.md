ARMtix package builder framework
--------------------------------

This repository contains scripts to set up and run
[ARMtix](https://armtix.artixlinux.org)
package builder framework for
[Mesos](https://mesos.apache.org/)

This framework is used to build ARMtix packages on Mesos cluster.
Both master and all the worker nodes have to be Aarch64 devices.

Contents
========
* docs - a directory with documentation for scripts and framework in general
* framework.py - main framework executable
* gittodb.py - a module to store git repositories information in MySQL database (other options can be added in future)
* scripts - a directory with currently implemented init scripts
* sqlite.tcl - a small script to access runtime SQLite database via HTTP
* sync.py - a script to sync current state of git repos with database
* wendy-host.sh - a wendy script which should be running on master node to add new packages to repos

Overwiew
========
A build cluster is created with Mesos. This framework is used to control package building tasks
and checking workers states. Each node builds only one package at a time. rsync is used to transfer data
between master and workers.
