#!/bin/python
"This is main sync executable. It should be wrapped with other script to be used in framework"
import argparse
import json
import os
import pycurl
import pygit2
import time
import gittodb

class curldata:
	def __init__(self):
		self.contents=''.encode('ascii')

	def data(self, buf):
		self.contents=self.contents+buf

parser = argparse.ArgumentParser()
parser.add_argument('--database', help = 'Database name',type=str,required=True)
parser.add_argument('--host', help = 'Database host address',type=str)
parser.add_argument('--port', help = 'Database port (default: 3306)',type=int,default=3306)
parser.add_argument('--unix', help = 'Unix socket for database',type=str)
parser.add_argument('--user', help = 'Database user to connect to',type=str,required=True)
parser.add_argument('--password', help = 'Users password if needed',type=str,default='')
parser.add_argument('--compress', help = 'Set True to avail compression')
args = parser.parse_args()

compress=False
if args.compress:
	compress=True
if args.unix:
	db=gittodb.GitToDB(args.user,args.password,args.database,unix_socket=args.unix,compress=compress)
elif args.host:
	db=gittodb.GitToDB(args.user,args.password,args.database,host=args.host,port=args.port,compress=compress)
else:
	print("Either database host or UNIX socket must be specified!", file=sys.stderr)
	sys.exit(1)

pwd=os.getcwd()
gitdirs=db.perform("SELECT path,kind,updatedate,url FROM gitdirs;")
nobuilds=[]
for i in db.perform("SELECT * FROM nobuild;"):
	nobuilds.append(i[0])
result=0
for gitdir in gitdirs:
	path=gitdir[0]
	kind=gitdir[1]
	updatedate=gitdir[2]
	baseurl=gitdir[3]
	if kind=="repo":
		continue
	elif kind=="gitea":
		os.chdir(path)
		data=curldata()
		curl=pycurl.Curl()
		curl.setopt(pycurl.HTTPHEADER, ["Content-Type: application/json", "Connection: close", "Accept: application/json"])
		curl.setopt(pycurl.WRITEFUNCTION, data.data)
		stop=0
		page=0
		while stop==0:
			page+=1
			url="%s?limit=400&page=%i"%(baseurl,page)
			curl.setopt(pycurl.URL, url)
			curl.perform()
			if data.contents.decode('utf8').split('\n')[-2] == '[]' or page==25: # stop if we reached the end or pages number exceeded for some reason
				if page==25:
					print("exceeded pages limit: %i"%(page))
					result+=1
				stop=1
		curl.close()
		jdata=[]
		for page in data.contents.decode('utf8').split('\n')[0:-1]:
			jdata+=json.loads(page)
		for i in range(0,len(jdata)):
			t=jdata[i]["updated_at"]
			#index=t.rindex(':')
			jdata[i]["updated_at"]=int(time.mktime(time.strptime(t, "%Y-%m-%dT%H:%M:%S%z")))
		jdata.sort(key=lambda x: x["updated_at"], reverse=True)
		dump=open("/tmp/gitea.json", "w")
		print(json.dumps(jdata), file=dump)
		dump.close()
		# iterate repos until updatedate is reached
		for pkg in jdata:
			if not(pkg["name"] in nobuilds) and not(pkg["name"][0:6] == "lib32-"):
				# for each repo, clone if it not exist, update otherwise
				try:
					os.stat(pkg["name"])
					#os.system("git -C %s checkout -q -f master"%(pkg["name"]))
					#result+=os.system("git -C %s pull"%(pkg["name"]))
					repo=pygit2.Repository(pkg["name"])
					commits=[commit for commit in repo.walk(repo.head.target, pygit2.GIT_SORT_TIME)]
					last_commit=commits[0]
					if last_commit.commit_time < pkg["updated_at"]:
						print(pkg["name"], " unsinked")
				except:
					#result+=os.system("git clone %s"%(pkg["clone_url"]))
					print(pkg["name"], " absent")
			# TODO: delete if needed
os.chdir(pwd)
