How to run build framework
--------------------------

Build framework relies on Mesos cluster to build packages. Example scripts for Mesos master and workers can be found in scripts/ directory.
Additionally you need MySQL database, rsyncd, HTTP server and SMTP server for operational framework.

Master node is not nesseseraly the same as the node framework is run on. It can also be not aarch64 node.

Framework node
==============

This node contains reference chroot in /armtix-master/chroot (hardcoded atm), thus a user running framework must be able to execute chroot-run without password.
This node can also be the one providing PKGBUILDs and providing main repo.

In order to handle incoming built packages, create /armtix-master/upload directory in which rsync will put packages and which will be monitored by wendy (see scripts/ directory).

Builder nodes
=============

These nodes must have /armtix-worker/mesos-worker and /armtix-worker/chroot directories. The username specified as --role framework argument must present on each node.
