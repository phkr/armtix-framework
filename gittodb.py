"""Database schema:
table nobuild: packages which should not be built
	name(str) - name of package
table gitdirs: git sources
	name(str) - name of repo
	path(str) - path to directory
	commit(str) - latest commit
	arm(str) - specifies ARM version
table chains: packages which must be built in certain order manually are listed here
	pkgname(str) - package name
	chain(str) - chain it belongs to
	commit(str) - latest commit for package
	source(str) - Artix repo it belongs to
	status(int) - status of package build
	armsource(str) - ARM patched PKGBUILDs repo
	arm(str) - latest commit with ARM patches, NULL if no patches required
table packages: main table, contains all info for build
	pkgname(str) - pkgbase or pkgname
	pkgver(str) - version of package
	provide(str) - comma-separated list of provided packages
	deps(str) - comma-separate dlist of dependencies
	makedeps(str) - comma-separated list of makedependencies
	repo(str) - system,world,galaxy
	source(str) - Artix repo of package
	commit(str) - latest commit, NULL if package is ARM-specific
	armsource(str) - ARM patched PKGBUILDs repo
	arm(str) - latest commit with ARM patches, NULL if no patches required
	date(int) - date of latest Artix commit in UNIX format
	base(int, 0 or 1) - if this package is assumed to be installed in base system and is not mentioned in deps
	status(int) - status of build

build statuses:
	0 - OK
	1 - failed
	2 - available
	3 - running
	4 - waits for dependency
	5 - waits for patch
	6 - waits for mainline commit (used if ARM patch came before Artix repos update)
	7 - waits build in chain
	8 - update exists, details are in process
"""
import pygit2
import MySQLdb.connections as DB
#import MySQLdb.cursors as cursors
import os, sys, subprocess, time, yaml

def parsePKGBUILD(path):
	"""Parses PKGBUILD file to obtain name, version, deps and list of provided packages"""
	cwd=os.getcwd() #get current work directory to return in the end
	pkginfo={"depends": [], "makedepends": [], "provides": []}
	try:
		os.chdir(path)
		srcinfo=subprocess.Popen(["makepkg", "--printsrcinfo"], stdout=subprocess.PIPE)
		for line in iter(srcinfo.stdout.readline, ''):
			if not(line): #othervise we get infinite loop
				break
			args=line.decode('utf8').split('\n')[0].split(' = ')
			arg=args[0].lstrip(' ').lstrip('\t')
			if arg=="depends": #do not check versions for (make)depends, may be changed in future if leads to errors
				dep=args[1].split('=')[0].split('>')[0].split('<')[0]
				if not(dep in pkginfo["depends"]):
					pkginfo["depends"].append(dep)
			elif arg=="makedepends" or arg=="checkdepends":
				makedep=args[1].split('=')[0].split('>')[0].split('<')[0]
				if not(makedep in pkginfo["makedepends"]):
					pkginfo["makedepends"].append(makedep)
			elif arg=="provides":
				pkginfo["provides"].append(args[1].split('=')[0])
			elif arg=="pkgname": #all pkgnames assigned with pkgbase go to provide section
				pkginfo["provides"].append(args[1])
			elif arg=="pkgbase":
				pkginfo["pkgbase"]=args[1]
			elif arg=="pkgver":
				pkginfo["pkgver"]=args[1]
			elif arg=="pkgrel":
				pkginfo["pkgrel"]=args[1]
		srcinfo.wait()
	except Exception as exc:
		print("error during processing makepkg, path: %s"%(os.getcwd()), file=sys.stderr)
		if hasattr(exc, 'message'):
			print(exc.message, file=sys.stderr)
		else:
			print(exc, file=sys.stderr)
		pkginfo={}

	os.chdir(cwd)
	if "pkgbase" in pkginfo:
		return pkginfo
	else:
		return {}

class GitToDB:
	"""
	This is supplementary class for ArtixARM mesos framework
	
	Its purpose is to establish connection between git directories and MySQL
	database.
	...
	Attributes
	----------
	db - connection
	"""
	def __init__(self, user, password, database, unix_socket=None, host=None, port=3306, compress=False):
		"""
		user - username
		password - user password
		database - database name
		unix_socket - path to UNIX socket to connect to
		host - address of server to connect to, doesn't work if unix_socket is specified
		port (optional) - port of server to connect to (default: 3306)
		compress (optional) - enable compression (default: False)
		Either host or unix_socket must be specified
		"""
		if unix_socket:
			self.db=DB.Connection(user=user, password=password, database=database, unix_socket=unix_socket, compress=compress)
		elif host:
			self.db=DB.Connection(user=user, password=password, database=database, host=host, port=port, compress=compress)
		else:
			print("you should connect to host or unix_socket", file=sys.stderr)
			sys.exit(1)

	def __del__(self):
		self.db.close()

	def add_gitdirs(self, dirs, arm=None):
		"""
		Adds git directories to list of tracked repositories.
		If arm is not None they are added as directories with patched PKGBUILDs (could be used further for different archs)
		[Dirs] is a list of tuples of form ('path', 'name', spec) describing git repos
		  here spec is the following object:
		    {'kind':kind,'url':url}
		    kind is either 'repo' or 'gitea'
		    if kind if gitea, url must be the url of API endpoint to list git repos in organization
		"""
		wd=os.getcwd()
		print("Adding gitdirs")
		for target in dirs:
			cursor=self.db.cursor()
			os.chdir(target[0])
			#proc=subprocess.Popen("find . ( -wholename \"*trunk/PKGBUILD\" ) -o ( -name \"PKGBUILD\" -print )", shell=False, stdout=subprocess.PIPE)
			proc=subprocess.Popen(["find", ".", "(", "-wholename", "*trunk/PKGBUILD", ")", "-o", "(", "-name", "PKGBUILD", "-print", ")"], shell=False, stdout=subprocess.PIPE)
			#proc.wait()
			scandirs=[]
			for line in iter(proc.stdout.readline, ''):#find all directories with PKGBUILDs
				if not(line):
					break
				dc=line.decode('utf8').split('\n')[0].split('/PKGBUILD')[0]
				if (arm or (not(("/testing-" in dc) or ("/staging-" in dc) or ("/community-staging" in dc) or ("/community-testing" in dc) or ("/multilib-" in dc) or ("/kde-unstable" in dc) or ("/rebuild/" in dc) or ("-rebuild/" in dc)))):
					#without testing/staging ones
					scandirs.append(dc)
			try:
				if target[2]["kind"] == "repo":
					cmdline=["/bin/sh", "-c", "git log --pretty=format:\"%H\" -n 1"]
					proc=subprocess.Popen(cmdline, stdout=subprocess.PIPE)#get latest commit for this repo
					proc.wait()
					commit=proc.stdout.readline().decode("utf8")
					cursor.execute("INSERT INTO gitdirs(name,path,kind,commit,arm) VALUES ('%s','%s','%s','%s', '%s');"%(target[1],target[0],target[2]["kind"],commit,arm))
				elif target[2]["kind"] == "gitea":
					cursor.execute("INSERT INTO gitdirs(name,path,kind,url,updatedate,arm) VALUES ('%s','%s','%s', '%s',%i, '%s');"%(target[1],target[0],target[2]["kind"],target[2]["url"],int(time.time()),arm))
				for cd in scandirs:
					if target[2]["kind"] == "repo":
						#cmdline=['git', 'log', '--pretty=format:\"\%H;\%\%ct\"', '-n', '1', '--', '%s/PKGBUILD'%(cd)]
						cmdline=["/bin/sh", "-c", "git log --pretty=format:\"%%H %%ct\" -n 1 -- %s/PKGBUILD"%(cd)]
					elif target[2]["kind"] == "gitea":
						dirname=cd.split('/')[0]+'/'+cd.split('/')[1]
						cmdline=["/bin/sh", "-c", "git -C %s log --pretty=format:\"%%H %%ct\" -n 1 -- %s/PKGBUILD"%(dirname, cd.removeprefix(dirname+"/"))]
					proc=subprocess.Popen(cmdline, stdout=subprocess.PIPE)
					proc.wait()
					commit,commitdate=proc.stdout.readline().decode("utf8").split(' ')#get latest commit and its date for this package
					commitdate=int(commitdate)
					repo=""#find a repo where we will save package
					if (cd.find("/galaxy/")>0 or cd.find("communty-x86_64")>0 or cd.find("community-any")>0 or cd.find("/community")>=0):
						repo="galaxy"
					elif (cd.find("/world/")>0 or cd.find("extra-x86_64")>0 or cd.find("extra-any")>0 or cd.find("/extra")>=0):
						repo="world"
					elif (cd.find("/system/")>0 or cd.find("core-x86_64")>0 or cd.find("core-any")>0 or cd.find("/core")>=0):
						repo="system"
					else:
						repo="armtix" #let's save it by this name
					pkginfo=parsePKGBUILD(cd) #get info about package
					if pkginfo:
						provides=""
						for line in pkginfo["provides"]:
							provides=provides+line+','
						provides=provides.rstrip(',')
						depends=""
						for line in pkginfo["depends"]:
							depends=depends+line+','
						depends=depends.rstrip(',')
						if depends is None:
							depends=""
						makedeps=""
						for line in pkginfo["makedepends"]:
							makedeps=makedeps+line+','
						makedeps=makedeps.rstrip(',')
						if makedeps is None:
							makedeps=""
						mode=0 #0 - insert, 1 - insert as ARM patched, 2 - add Artix repo to package with patch, 3 - add ARM patch to Artix package
						cursor.execute("SELECT arm FROM packages WHERE pkgname='%s';"%(pkginfo["pkgbase"]))
						out=cursor.fetchall()
						if (len(out)==0 and arm):
							mode=1
						elif (len(out)!=0 and not(arm)):
							mode=2
						elif (len(out)!=0):
							mode=3
						if (mode==0):#OK, add changes
							cursor.execute("INSERT INTO packages(pkgname,pkgver,provide,deps,makedeps,source,repo,commit,date,status) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',%i,%i);"%(pkginfo["pkgbase"],"%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]),provides,depends,makedeps,target[1],repo,commit,commitdate,0))
						elif (mode==1):
							cursor.execute("INSERT INTO packages(pkgname,pkgver,provide,deps,makedeps,armsource,repo,arm,status) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',%i);"%(pkginfo["pkgbase"],"%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]),provides,depends,makedeps,target[1],repo,commit,0))
						elif (mode==2):
							cursor.execute("UPDATE packages SET source='%s',commit='%s',date='%i' WHERE pkgname='%s';"%(target[1],commit,commitdate,pkginfo["pkgbase"])) #assume repos are synced
						elif (mode==3):
							cursor.execute("UPDATE packages SET armsource='%s',arm='%s' WHERE pkgname='%s';"%(target[1],commit,pkginfo["pkgbase"])) #assume repos are synced
				self.db.commit()
			except Exception as exc:
				print("failed to process %s"%target[1], file=sys.stderr)
				self.db.rollback()
				if hasattr(exc, 'message'):
					print(exc.message, file=sys.stderr)
				else:
					print(exc, file=sys.stderr)
			cursor.close()
			proc.wait()
			os.chdir(wd)

	def add_chains(self, chains):
		"""
		Adds new package chains. All packages must exist in database
		{Chains} is a dict of lists of packages which must be built in certain order
		form: "chain_name": [<names of packages>]
		"""
		cursor=self.db.cursor()
		ret=0
		print("Adding chains")
		try:
			for chain in list(chains.keys()):
				for pkgname in chains[chain]:
					cursor.execute("SELECT source,commit,armsource,arm FROM packages WHERE pkgname='%s';"%pkgname)
					out=cursor.fetchall()[0]
					if (len(out) == 0):
						print("Could not create chain %s: package %s does not exist"%(chain, pkgname), file=sys.stderr)
						ret=1
						break
					elif (out[1] and out [3]):
						cursor.execute("INSERT INTO chains(pkgname,chain,commit,source,arm,armsource,status) VALUES ('%s','%s','%s','%s','%s','%s',%i);"%(pkgname,chain,out[1],out[0],out[3],out[2],0))
					elif out[1]:
						cursor.execute("INSERT INTO chains(pkgname,chain,commit,source,status) VALUES ('%s','%s','%s','%s',%i);"%(pkgname,chain,out[1],out[0],0))
					elif out[3]:
						cursor.execute("INSERT INTO chains(pkgname,chain,arm,armsource,status) VALUES ('%s','%s','%s','%s',%i);"%(pkgname,chain,out[3],out[2],0))
				if ret:
					raise Exception
			self.db.commit()
			print("Chains added")
		except Exception as exp:
			print("Failed to add chains", file=sys.stderr)
			self.db.rollback()
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
		cursor.close()

	def add_nobuild(self, nobuild):
		"""
		Adds packages to the nobuild list
		[Nobuild] is a list of packages which shouldn't be built at all
		"""
		cursor=self.db.cursor()
		try:
			print('Adding nobuild entries')
			for val in nobuild:
				cursor.execute("INSERT INTO nobuild VALUES ('%s');"%(val))
			self.db.commit()
			print("Nobuild entries added")
		except Exception as exc:
			print("Failed to add nobuild entries", file=sys.stderr)
			self.db.rollback()
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
		cursor.close()

	def change_nobuild(self, nobuild=[]):
		"""
		Sets the nobuild list
		[Nobuild] is a list of packages which shouldn't be built at all
		"""
		cursor=self.db.cursor()
		try:
			print('Clearing nobuild')
			cursor.execute("DELETE FROM nobuild;")
			print('Populating nobuild')
			for val in nobuild:
				cursor.execute("INSERT INTO nobuild VALUES ('%s');"%(val))
			self.db.commit()
			print("Nobuilds set")
		except Exception as exc:
			print("Failed to set nobuild", file=sys.stderr)
			self.db.rollback()
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
		cursor.close()

	def create_db(self):
		"""Creates tables"""
		ret=0
		cursor=self.db.cursor()
		try:
			print("Creating tables")
			cursor.execute("CREATE TABLE nobuild(name VARCHAR(127) PRIMARY KEY);")
			cursor.execute("CREATE TABLE gitdirs(name VARCHAR(127), path VARCHAR(511), kind TINYTEXT, commit TINYTEXT, url TINYTEXT, updatedate INT, arm TINYTEXT, PRIMARY KEY (name, path));")
			cursor.execute("CREATE TABLE packages(pkgname VARCHAR(127), pkgver TINYTEXT NOT NULL, provide MEDIUMTEXT NOT NULL, deps MEDIUMTEXT NOT NULL, makedeps MEDIUMTEXT NOT NULL, repo TINYTEXT NOT NULL, source TINYTEXT DEFAULT NULL, commit TINYTEXT DEFAULT NULL, date INT DEFAULT NULL, armsource TINYTEXT DEFAULT NULL, arm TINYTEXT DEFAULT NULL, status INT NOT NULL CHECK(status>=0 AND status<=8), base INT NOT NULL DEFAULT 0 CHECK (base IN (0,1)), PRIMARY KEY (pkgname)), CHECK(status>=0 AND status<=8);")
			cursor.execute("CREATE TABLE chains(pkgname VARCHAR(127), chain VARCHAR(127), commit TINYTEXT DEFAULT NULL, source TINYTEXT DEFAULT NULL, arm TINYTEXT DEFAULT NULL, armsource TINYTEXT DEFAULT NULL, status INT NOT NULL, PRIMARY KEY (pkgname, chain), FOREIGN KEY(pkgname) REFERENCES packages(pkgname));")
			self.db.commit()
			print("Tables created")
		except Exception as exc:
			self.db.rollback()
			print("Operation failed, aborting", file=sys.stderr)
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
			ret=1
		cursor.close()
		if ret:
			sys.exit(1)

	def perform(self, query):
		"""Send a query to database"""
		ret=0
		cursor=self.db.cursor()
		try:
			cursor.execute(query)
			out=cursor.fetchall()
			self.db.commit()
		except Exception as exc:
			print("Failed to execute query", file=sys.stderr)
			self.db.rollback()
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
			ret=1
		cursor.close()
		if (not(ret)):
			ret=out
		return ret

	def remove_chains(self, chains):
		"""
		Removes chains
		[Chains] is a list of chains to delete
		"""
		ret=0
		cursor=self.db.cursor()
		try:
			print("Removing chains")
			for chain in chains:
				cursor.execute("DELETE FROM chains WHERE chain='%s';"%chain)
		except Exception as exc:
			ret=1
			self.db.rollback()
			print("failed to delete chains", file=sys.stderr)
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
		if not(ret):
			self.db.commit()
			print("Chains deleted")
		cursor.close()

	def remove_gitdirs(self, dirs):
		"""
		Do not track specified directories anymore. All related packages will be removed
		[Dirs] is a list of names of repositories to remove
		"""
		ret=0
		cursor=self.db.cursor()
		print("Removing gitdirs")
		try:
			for dirname in dirs:
				cursor.execute("SELECT arm FROM gitdirs WHERE name='%s';"%dirname)#check if dir is for patches
				arm=cursor.fetchall()[0][0]
				if (arm != 'None'):#get list of packages
					cursor.execute("SELECT pkgname FROM packages WHERE armsource='%s';"%dirname)
				else:
					cursor.execute("SELECT pkgname FROM packages WHERE source='%s';"%dirname)
				packages=cursor.fetchall()
				for pkg in packages:
					pkg=pkg[0]
					if (arm != 'None'):#for arm check if there is mainline package
						cursor.execute("SELECT source FROM packages WHERE pkgname='%s';"%pkg)
					else:#check for patches
						cursor.execute("SELECT armsource FROM packages WHERE pkgname='%s';"%pkg)
					src=cursor.fetchall()[0][0]
					if not(src):#if no other type of source, delete package
						cursor.execute("DELETE FROM chains WHERE pkgname='%s';"%pkg)
						cursor.execute("DELETE FROM packages WHERE pkgname='%s';"%pkg)
					elif (arm != 'None'):#if there is other type of source, remove current type of source
						cursor.execute("UPDATE chains SET armsource=NULL,arm=NULL WHERE pkgname='%s';"%pkg)
						cursor.execute("UPDATE packages SET armsource=NULL,arm=NULL WHERE pkgname='%s';"%pkg)
					else:
						cursor.execute("UPDATE chains SET commit=NULL,source=NULL WHERE pkgname='%s';"%pkg)
						cursor.execute("UPDATE packages SET commit=NULL,source=NULL,date=NULL WHERE pkgname='%s';"%pkg)
				cursor.execute("DELETE FROM gitdirs WHERE name='%s';"%dirname)
			self.db.commit()
			print("Gitdirs removed")
		except Exception as exc:
			print("Failed to remove dirs", file=sys.stderr)
			self.db.rollback()
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)

	def sync(self, fromzero=False, debug=False):
		"""
		Syncronise current state of git dirs with database
		If fromzero is set to False, it will only scan gitea gitdirs for which .artixlinux/pkgbase.yaml was updated after known gitdir updatedate; otherwise scans all
		"""
		cursor=self.db.cursor()
		wd=os.getcwd()
		try:
			cursor.execute("SELECT * FROM nobuild;")
			nobuild=cursor.fetchall()
			nobuilds=[]
			for pkg in nobuild:
				nobuilds.append(pkg[0])
			cursor.execute("SELECT name,path,kind,commit,updatedate,arm FROM gitdirs;")
			gitdirs=cursor.fetchall()
			for gitdir in gitdirs:#scanning gitdirs
				name=gitdir[0]
				path=gitdir[1]
				kind=gitdir[2]
				build_commit=gitdir[3]
				updatedate=gitdir[4]
				arm=gitdir[5]
				pkgs={}
				print("Processing %s"%name)
				if kind == "repo":
					gitpaths=[path]
				elif kind == "gitea":
					gitpaths=[]
					for listdir in os.listdir(path):
						try:
							cursor.execute("SELECT date FROM packages WHERE pkgname='%s';"%(listdir))
							try:
								resp=cursor.fetchall()
								pkgdate=resp[0][0]
							except:
								pkgdate=0
							if os.path.isdir(path+'/'+listdir) and not(listdir in nobuilds):
								repo=pygit2.Repository(path+'/'+listdir)
								if repo[repo.head.target].commit_time > pkgdate or fromzero:
									gitpaths.append(path+'/'+listdir)
						except:
							print("failed processing on %s"%(listdir))
				for gitpath in gitpaths:
					os.chdir(gitpath)
					repo=pygit2.Repository(gitpath)
					if kind == "gitea":
						#pkginfo=parsePKGBUILD(gitpath+"/trunk")
						#pkgbase=pkginfo["pkgbase"]
						pkgbase=gitpath.split('/')[-1]
						cursor.execute("SELECT commit FROM packages WHERE pkgname='%s';"%(pkgbase))
						out=cursor.fetchall()
						if len(out)>0:
							build_commit=out[0][0]
						else:
							build_commit="nocommit"
						# skip lib32 processing
						if pkgbase[0:6] == "lib32-":
							continue
					commits=[commit for commit in repo.walk(repo.head.target, pygit2.GIT_SORT_TIME)]
					for i in range(0,len(commits)):#obtain commits
						if commits[i].id == build_commit:
							break
					commits=commits[:i+1:]
					if (len(commits)==1):#if we are on HEAD, start next iteration
						os.chdir(wd)
						continue
					if kind == "gitea":
						startlen=0 # set to 0 to process all commits HEAD...last_known
						commits=commits[:len(commits)-1:] # exclude last known commit
					else:
						startlen=1 # set to 1 to have smth to compare to
						commits=commits[::-1] # inverse commits
					for commit in range(startlen,len(commits)):#process commits
						commit_hash=commits[commit].id
						commit_time=commits[commit].commit_time
						if kind == "gitea":
							if commit_time > updatedate:
								updatedate=commit_time
						pkginfo={}
						repo=""
						if kind == "gitea":
							# parse pkgbase.yaml
							if debug:
								print("checking out %s %s"%(gitpath, commit_hash))
							os.system("git -C %s checkout -q -f %s"%(gitpath, commit_hash))
							try:
								pkgyaml=open("%s/.artixlinux/pkgbase.yaml"%(gitpath), "r")
								try:
									pkgyamlcontents = yaml.safe_load(pkgyaml)
									repo = "broken"
									if pkgyamlcontents["actions"]["addRepo"] == "system":
										#if (pkgyamlcontents["repos"]["system-gremlins"]["version"] == None or \
										#  pkgyamlcontents["repos"]["system-gremlins"]["version"] == pkgyamlcontents["repos"]["system"]["version"]) and \
										#  (pkgyamlcontents["repos"]["system-goblins"]["version"] == None or \
										#  pkgyamlcontents["repos"]["system-goblins"]["version"] == pkgyamlcontents["repos"]["system"]["version"]):
										if pkgyamlcontents["repos"]["system"]["version"] != None:
											repo="system"
									elif pkgyamlcontents["actions"]["addRepo"] == "world":
										#if (pkgyamlcontents["repos"]["world-gremlins"]["version"] == None or \
										#  pkgyamlcontents["repos"]["world-gremlins"]["version"] == pkgyamlcontents["repos"]["world"]["version"]) and \
										#  (pkgyamlcontents["repos"]["world-goblins"]["version"] == None or \
										#  pkgyamlcontents["repos"]["world-goblins"]["version"] == pkgyamlcontents["repos"]["world"]["version"]):
										if pkgyamlcontents["repos"]["world"]["version"] != None:
											repo="world"
									elif pkgyamlcontents["actions"]["addRepo"] == "galaxy":
										#if (pkgyamlcontents["repos"]["galaxy-gremlins"]["version"] == None or \
										#  pkgyamlcontents["repos"]["galaxy-gremlins"]["version"] == pkgyamlcontents["repos"]["galaxy"]["version"]) and \
										#  (pkgyamlcontents["repos"]["galaxy-goblins"]["version"] == None or \
										#  pkgyamlcontents["repos"]["galaxy-goblins"]["version"] == pkgyamlcontents["repos"]["galaxy"]["version"]):
										if pkgyamlcontents["repos"]["galaxy"]["version"] != None:
											repo="galaxy"
									if repo != "broken":
										pkginfo=parsePKGBUILD(gitpath)
								except Exception as e:
									print("failed to parse yaml for %s at %s: %s"%(pkgbase, commit_hash, str(e)))
								pkgyaml.close()
							except Exception as e:
								print("failed to open pkgbase yaml for %s: %s"%(pkgbase, str(e)))
							if debug:
								print("checking out %s master"%(gitpath))
							os.system("git -C %s checkout -q -f master"%(gitpath))
						else:
							for patch in commits[commit].tree.diff_to_tree(commits[commit-1].tree):#parsing commit patches, if there is PKGBUILD, add it to list
								# parse PKGBUILD
								data=patch.text.splitlines()[0].split(' a/')[1].split(' b/')[0]
								if data.split('/')[-1] == "PKGBUILD" and os.path.isfile(data):
									# currently it is only meant for ARMtix
									if data.find("galaxy/")>=0 or data.find("/communty-x86_64/")>0 or data.find("/community-any/")>0 or data.find("/community/")>0:
										repo="galaxy"
									elif data.find("world/")>=0 or data.find("/extra-x86_64/")>0 or data.find("/extra-any/")>0 or data.find("/extra/")>0:
										repo="world"
									elif data.find("system/")>=0 or data.find("/core-x86_64/")>0 or data.find("/core-any/")>0 or data.find("/core/")>0:
										repo="system"
									else:
										repo="armtix" #let's save it by this name
									pkginfo=parsePKGBUILD(data.split('/PKGBUILD')[0])
									break

						if "pkgbase" in pkginfo and repo != 'broken':
							if kind=="repo":
								# pkgbase is defined for gitea already
								pkgbase=pkginfo["pkgbase"]
							provides=""
							for line in pkginfo["provides"]:
								provides=provides+line+','
							provides=provides[:-1:]
							depends=""
							for line in pkginfo["depends"]:
								depends=depends+line+','
							depends=depends[:-1:]
							makedeps=""
							for line in pkginfo["makedepends"]:
								makedeps=makedeps+line+','
							makedeps=makedeps[:-1:]
							cursor.execute("SELECT source,armsource,status,pkgver FROM packages WHERE pkgname='%s';"%pkgbase)
							srcs=cursor.fetchall()
							status=8 #by default set package to process
							chains=[] #package is not in chain
							#check if package is in chain
							cursor.execute("SELECT chain FROM chains WHERE pkgname='%s';"%pkgbase)
							chains=cursor.fetchall()
							if len(srcs)==0:#we have new package
								if (arm and arm != 'None'):
									cursor.execute("INSERT INTO packages(pkgname,pkgver,provide,deps,makedeps,armsource,repo,arm,status) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',%i);"%(pkgbase,"%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]),provides,depends,makedeps,name,repo,commit_hash,8))
								else:
									cursor.execute("INSERT INTO packages(pkgname,pkgver,provide,deps,makedeps,source,repo,commit,date,status) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',%i,%i);"%(pkgbase,"%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]),provides,depends,makedeps,name,repo,commit_hash,commit_time,8))

							elif srcs[0][3] == "%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]) and srcs[0][2] == 0:#attempt to update already built package version, skip
								if debug:
									print("skipping package %s: version %s in db, %s tried to push"%(pkgbase, srcs[0][3], "%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"])))

							elif arm != 'None':#we work with ARM
								if (srcs[0][0] and srcs[0][0] != 'None' and int(srcs[0][2]) != 5): #patch has come but mainline commit didn't: set waiting state
									status=6
								cursor.execute("UPDATE packages SET pkgver='%s',provide='%s',deps='%s',makedeps='%s',repo='%s',arm='%s',status=%i,armsource='%s' WHERE pkgname='%s';"%("%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]),provides,depends,makedeps,repo,commit_hash,status,name,pkgbase))

							else:#we work with Artix
								if (srcs[0][1] and srcs[0][1] != 'None' and int(srcs[0][2]) != 6): #mainline commit has come, waiting for patch
									status=5
								if (srcs[0][1] and srcs[0][1] != 'None'):#if there is also ARM source, don't change deps
									cursor.execute("UPDATE packages SET pkgver='%s',commit='%s',repo='%s',date=%i,status=%i,source='%s' WHERE pkgname='%s';"%("%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]),commit_hash,repo,commit_time,status,name,pkgbase))
								else:#otherwise update them too
									cursor.execute("UPDATE packages SET pkgver='%s',provide='%s',deps='%s',makedeps='%s',commit='%s',repo='%s',date=%i,status=%i,source='%s' WHERE pkgname='%s';"%("%s-%s"%(pkginfo["pkgver"],pkginfo["pkgrel"]),provides,depends,makedeps,commit_hash,repo,commit_time,status,name,pkgbase))
							if status==8 and len(chains)>0:#package is in chain and ready to be built
								cursor.execute("UPDATE packages SET status=7 WHERE pkgname='%s';"%(pkgbase))
								cursor.execute("UPDATE chains SET status=7 WHERE pkgname='%s';"%(pkgbase))
								for chain in chains:
									if (srcs[0][0] and srcs[0][0]!='None'):
										cursor.execute("SELECT commit FROM packages WHERE pkgname='%s'"%pkgbase)
										saved_hash=cursor.fetchall()[0][0]
										cursor.execute("UPDATE chains SET commit='%s' WHERE chain='%s' AND pkgname='%s';"%(saved_hash,chain[0],pkgbase))
									if (srcs[0][1] and srcs[0][1]!='None'):
										cursor.execute("SELECT arm FROM packages WHERE pkgname='%s'"%pkgbase)
										saved_hash=cursor.fetchall()[0][0]
										cursor.execute("UPDATE chains SET arm='%s' WHERE chain='%s' AND pkgname='%s';"%(saved_hash,chain[0],pkgbase))
							#if package is in nobuilds, set status=0
							if pkgbase in nobuilds:
								cursor.execute("UPDATE packages SET status=0 WHERE pkgname='%s';"%pkgbase)
							# for artix we only need latest stable commit for repo
							if kind == "gitea":
								break
					if kind == "repo":
						if debug:
							print("setting repo %s comit to %s"%(name, commits[len(commits)-1].id))
						cursor.execute("UPDATE gitdirs SET commit='%s' WHERE name='%s';"%(commits[len(commits)-1].id,name))
					elif kind == "gitea":
						os.system("git -C %s checkout -q -f master"%(gitpath))
						cursor.execute("UPDATE gitdirs SET updatedate=%i WHERE name='%s';"%(updatedate,name))
					os.chdir(wd)
			self.db.commit()
			print("Sync complete")
		except Exception as exc:
			self.db.rollback()
			print("Sync failed", file=sys.stderr)
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
		os.chdir(wd)
		cursor.close()

	def update(self):
		"""
		Check if there is a new portion of packages to be built
		The package is added to list of builds if it has all needed commits and all deps are built
		"""
		cursor=self.db.cursor()
		print("Updating packages status")
		try:
			cursor.execute("SELECT provide FROM packages WHERE status!=0 AND status!=4 AND status!=8;")
			unavail=cursor.fetchall()#these packages are not built
			provides_unavail=""#get list of names provided by unavailable packages
			for pkg in unavail:
				provides_unavail=provides_unavail+pkg[0]+','
			provides_unavail=list(set(provides_unavail[:-1:].split(',')))
			#we assume later depend on earler if there is a cycle
			cursor.execute("SELECT pkgname,provide,deps,makedeps,base,date FROM packages WHERE status=4 OR status=8 ORDER BY date;")
			expect=list(cursor.fetchall())#these packages are waiting the queue
			i=0
			length=len(expect)
			statuses={}#we will keep other packages in dictionary "pkg": status
			for i in range(0,length):
				statuses[expect[i][0]]=2#by default all are available
			for i in range(0,length):#first mark those which depend on processed from list
				for dep in expect[i][2].split(',')+expect[i][3].split(','):
					if dep in provides_unavail:
						statuses[expect[i][0]]=4
						break
			#if there is not built base package, all subsequent must depend on it
			cursor.execute("SELECT date FROM packages WHERE status!=0 AND base=1 ORDER BY date LIMIT 1;")
			basedate=cursor.fetchall()
			if len(basedate) > 0:
				basedate=basedate[0][0]
				if not(basedate):
					basedate=0
				for i in range(0,length):
					if expect[i][5]:
						if expect[i][5] > basedate:
							statuses[expect[i][0]]=4
			
			for i in range(0,length):
				for j in range(0,i):
					#if package is in base all the following depend on it
					if expect[j][4]==1 and expect[i][4]==0:
						statuses[expect[i][0]]=4
						break
				if statuses[expect[i][0]]==4:
					continue
				for j in range(0, length):
					if i==j:
						continue
					else:
						i_on_j=False#means that i-th package depends on this j-th
						j_on_i=False#j-th depends on i-th
						depsi=expect[i][2].split(',')+expect[i][3].split(',')
						depsj=expect[j][2].split(',')+expect[j][3].split(',')
						provsi=expect[i][1].split(',')
						provsj=expect[j][1].split(',')
						for dep in depsi:
							if dep in provsj:
								#i-th depends on j-th
								i_on_j=True
								break
						for dep in depsj:
							if dep in provsi:
								#j-th depends on i-th
								j_on_i=True
								break
						if i_on_j and j_on_i:
							#the one commited earlier will be built
							if i<j:
								statuses[expect[j][0]]=4
							else:
								statuses[expect[i][0]]=4
						elif i_on_j:
							#only build j
							statuses[expect[i][0]]=4
						elif j_on_i:
							#only build i
							statuses[expect[j][0]]=4
			#now set statuses
			for i in range(0,length):
				cursor.execute("UPDATE packages SET status=%i WHERE pkgname='%s';"%(statuses[expect[i][0]],expect[i][0]))
			self.db.commit()
			print("Database updated")
		except Exception as exc:
			self.db.rollback()
			print("Failed to update database", file=sys.stderr)
			if hasattr(exc, 'message'):
				print(exc.message, file=sys.stderr)
			else:
				print(exc, file=sys.stderr)
		cursor.close()
