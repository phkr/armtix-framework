#!/bin/python

import gittodb
import argparse

parser = argparse.ArgumentParser()
# package to search
parser.add_argument('--package', help = 'Package to search',type=str,required=True)
#Database connection options
parser.add_argument('--database', help = 'Database name',type=str,required=True)
parser.add_argument('--host', help = 'Database host address',type=str)
parser.add_argument('--port', help = 'Database port (default: 3306)',type=int,default=3306)
parser.add_argument('--unix', help = 'Unix socket for database',type=str)
parser.add_argument('--user', help = 'Database user to connect to',type=str,required=True)
parser.add_argument('--password', help = 'Users password if needed',type=str,default='')
parser.add_argument('--compress', help = 'Set True to avail compression',default=False)
#misc
parser.add_argument('--debug', help = 'Enable debug messages')
args = parser.parse_args()

db=gittodb.GitToDB(args.user, args.password, args.database, unix_socket=args.unix, host=args.host, port=args.port, compress=args.compress)

deps_string=db.perform("SELECT deps FROM packages WHERE pkgname='%s';"%(args.package))[0][0]
makedeps_string=db.perform("SELECT makedeps FROM packages WHERE pkgname='%s';"%(args.package))[0][0]

try:
	deps_list=deps_string.split(',')
except:
	deps_list=[]

try:
	makedeps_list=makedeps_string.split(',')
except:
	makedeps_list=[]

deps_list=list(set(deps_list+makedeps_list))

for dep in deps_list:
	provides=db.perform("SELECT pkgname, status FROM packages WHERE status!=0 AND (provide='%s' OR provide LIKE '%%,%s' OR provide LIKE '%s,%%' OR provide LIKE '%%,%s,%%');"%(dep,dep,dep,dep))
	for provide in provides:
		print(provide)
