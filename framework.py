#!/bin/python
"""
sqlite database:
table state:
	format: name value
	framework - framework id
	state - framework state: running/paused/stopped
	sync - sync state: needed/running/finished/aborted
	update - update state: needed/running/finished/aborted
table tasks (external interface to control package building process):
	format: task_id, pkgname, agent_id, state
	states:
		0 - running
		1 - must be killed
table agents:
	format: agent_id, host. state
	states:
		0 - OK
		1 - suspended
this should be used to control framework via external interface
"""
import datetime
import gittodb
import argparse
import base64
from io import BytesIO
import json
import os
import pycurl
import random
import shutil
import sqlite3
import string
import sys

class workclass:
	"""
	Main working class.

	Its object is expected to be a Mesos "listener". It performs all of the scheduling logic.
	...
	Attributes
	----------
	sqlite_db, sqlite - connection to sqlite database
	db - connection to database
	curl - curl instance for sending messages to Mesos
	smtp - SMTP message sending interface
	stream_id - Mesos stream id used for performing calls
	fw_id - framework id
	agents - dict of agents with their states; state is one of: OK, suspended
	offers - dict of offers in a form of dictionary with ids as keys; data: agent_id, resources
	tasks - dict of tasks in a form of dictionary with ids as keys; data: state (correspons to Mesos task states), type (update or package), unres_offer, pkg, agent_id, resources
	rescinds - list of latest rescinded offers
	list - list of packages available for build
	sync - pid of sync process, 0 otherwise
	beats - number of HEARTBEAT messages, used as time measurement
	sync_cmd - path to command performing sync
	update - state of chroot update: 0 - finished, 1 - processing, 2 - needed, 3 - error, 4 - just finished
	update_pid - PID of chroot update process (0 - otherwise)
	"""
	def __init__(self, role, sqlite, sync_cmd, url, rsync_chroot, rsync_upload, ndp, pdp, mh, ma, mu, db_user, db_password, db_name, db_unix=None, db_host=None, db_port=3306, db_compress=False,debug=False):
		"""
		role - role on Mesos cluster
		sqlite - path to sqlite state database
		sync_cmd - path to command performing sync
		url - Mesos master URL
		rsync_chroot - address of reference chroot shared via rsync
		rsync_upload - describes where to upload package files (i.e. rsync://rsync_upload/repo/pkgname)
		npd - normal download path
		pdp - patched download path
		mh - SMTP mail host
		ma - address to send messages to
		mu - mail user to send messages from
		db_user - database user name
		db_password - user password
		db_name - database name
		db_unix - path to UNIX socket to connect to
		db_host - address of server to connect to, doesn't work if unix_socket is specified
		db_port (optional) - port of server to connect to (default: 3306)
		db_compress (optional) - enable compression (default: False)
		debug - enable debug messages
		Either host or unix_socket must be specified
		"""
		self.role=role
		self.sync_cmd=sync_cmd
		self.sqlite_db=sqlite3.connect(sqlite)
		self.sqlite=sqlite3.Cursor(self.sqlite_db)
		self.rsync_chroot=rsync_chroot
		self.rsync_upload=rsync_upload
		self.db=gittodb.GitToDB(db_user, db_password, db_name, unix_socket=db_unix, host=db_host, port=db_port, compress=db_compress)
		self.curl=pycurl.Curl()
		self.curl.setopt(pycurl.URL, "http://%s/api/v1/scheduler"%url)
		self.stream_id=''.encode('utf8')
		self.fw_id=''.encode('utf8')
		self.offers={}
		self.rescinds=[]
		self.tasks={}
		self.agents={}
		self.list=[]
		self.sync=0
		self.update=2
		self.update_pid=0
		self.beats=0
		self.ndp=ndp
		self.pdp=pdp
		self.ma=ma
		self.mh=mh
		self.mu=mu
		self.debug=debug
		#Setting up mail
		self.smtp=pycurl.Curl()
		self.smtp.setopt(pycurl.URL, "smtp://%s"%self.mh)
		#Set "available" status for packages marked as "running"
		self.db.perform("UPDATE packages SET status=2 WHERE status=3")
		#set permissions on sqlite db
		os.chmod(sqlite, 0o660)
		os.chmod(os.path.dirname(sqlite), 0o770)

	def headerfunction(self, data):
		"""
		This function extracts headers from Mesos stream. Its purpose is to determine Mesos stream id
		It is called by curl with HEADERFUNCTION option
		"""
		data=data.decode('utf8').split(': ')
		if (data[0]=="Mesos-Stream-Id"):
			self.stream_id=data[1].split('\r')[0]

	def writefunction(self, data):
		"""
		Main working function. It is called on each message from Mesos.
		All scheduling logic is implemented here
		"""
		#get framework state from sqlite DB
		fwstate="running"
		try:
			self.sqlite.execute("SELECT value FROM state WHERE name='state';")
			fwstate=self.sqlite.fetchone()[0]
		except:
			fwstate="paused"

		decoded_data=data.decode('utf8').split('\n')
		#for data in decoded_data[1::2]:
		if debug:
			print(decoded_data)
		for data in decoded_data:
			if self.debug:
				print(data, file=sys.stderr)
			try:
				jdata=json.loads(data[data.find('{'):data.rfind('}')+1])
				if debug:
					print("DEBUG: successful json decode", file=sys.stderr)
			except:
				# it should only happen for numeric-only message, no partial jsons expected
				if debug:
					print("DEBUG: shit happened", file=sys.stderr)
					print(data[data.find('{'):data.rfind('}')+1], file=sys.stderr)
				continue
			if (jdata["type"]=="HEARTBEAT"):
				self.beats+=1
				if self.debug:
					print("number of beats: ", self.beats)

			elif (jdata["type"]=="UPDATE"):
				#we received status update for certain task
				task_id=jdata["update"]["status"]["task_id"]["value"]
				state=jdata["update"]["status"]["state"]
				if task_id in self.tasks:
					self.tasks[task_id]["state"]=state
					data={"framework_id": {"value": self.fw_id},
						"type": "ACKNOWLEDGE",
						"acknowledge": {
							"agent_id": jdata["update"]["status"]["agent_id"],
							"task_id": jdata["update"]["status"]["task_id"]
						}
					}
					if "uuid" in jdata["update"]["status"].keys():
						data["acknowledge"]["uuid"]=jdata["update"]["status"]["uuid"]
					self.curl.setopt(pycurl.POSTFIELDS, json.dumps(data))
					self.curl.setopt(pycurl.HTTPHEADER, ["Content-Type: application/json", "Mesos-Stream-Id: %s"%self.stream_id])
					self.curl.perform()
					if self.debug:
						print("response code: ", self.curl.getinfo(pycurl.RESPONSE_CODE))
					# processing states
					task=""
					#TODO: status processing
					if state=="TASK_FINISHED":
						#succesful finish
						task=self.tasks.pop(task_id)
						self.db.perform("UPDATE packages SET status=0 WHERE pkgname='%s';"%task["pkg"])
						self.sqlite.execute("DELETE FROM tasks WHERE task_id='%s';"%task_id)
						self.sqlite_db.commit()
						self.send_mail(task_id, "package", "succeed", task["pkg"], task["repo"])
						if self.update==4:
							self.update=0
					elif state=="TASK_FAILED" or state=="TASK_KILLED" or state=="TASK_ERROR":
						#fail
						task=self.tasks.pop(task_id)
						self.db.perform("UPDATE packages SET status=1 WHERE pkgname='%s';"%task["pkg"])
						self.sqlite.execute("DELETE FROM tasks WHERE task_id='%s';"%task_id)
						self.sqlite_db.commit()
						self.send_mail(task_id, "package", "failed", task["pkg"], task["repo"])
					elif state=="TASK_UNREACHABLE":
						#mark as error but don't remove if it's package
						self.db.perform("UPDATE packages SET status=1 WHERE pkgname='%s';"%self.tasks[task_id]["pkg"])
						self.send_mail(task_id, "package", "lost", task["pkg"], task["repo"])
						#TODO: it can be returned with TASK_RUNNING
					elif state=="TASK_LOST" or state=="TASK_DROPPED" or state=="TASK_GONE":
						#restart it
						task=self.tasks.pop(task_id)
						self.db.perform("UPDATE packages SET status=2 WHERE pkgname='%s';"%task["pkg"])
						self.sqlite.execute("DELETE FROM tasks WHERE task_id='%s';"%task_id)
						self.sqlite_db.commit()
						self.send_mail(task_id, "package", "dropped", task["pkg"], task["repo"])
						#TODO: it can be returned with TASK_RUNNING, agent sends FAILURE w/o RESCIND

			elif (jdata["type"]=="OFFERS"):
				#list of offers has arrived
				declines=[]
				for offer in jdata["offers"]["offers"]:
					#if this agent is still connected, remove it from rescinds
					if offer["agent_id"]["value"] in self.rescinds:
						self.rescinds.pop(self.rescinds.index(offer["agent_id"]["value"]))
					#check if it is new agent
					if not(offer["agent_id"]["value"] in list(self.agents.keys())):
						#this is new agent
						self.agents[offer["agent_id"]["value"]]="OK"
						self.sqlite.execute("INSERT INTO agents VALUES('%s', '%s', 0);"%(offer["agent_id"]["value"], offer["hostname"]))
						self.sqlite_db.commit()
					# conditions to decline offer:
					# - there is nothing to build
					# - framework is paused
					# - offer provides too low CPU, RAM or disk
					# - update or sync is running
					# - agent is suspended
					res={}
					for resource in offer["resources"]:
						if resource["name"]=="cpus":
							res["cpus"]=resource["scalar"]["value"]
						elif resource["name"]=="mem":
							res["mem"]=resource["scalar"]["value"]
						elif resource["name"]=="disk":
							res["disk"]=resource["scalar"]["value"]
					if res["cpus"]<4 or res["mem"]<2000 or res["disk"]<10240 or len(self.list)==0 or fwstate=="paused" or self.sync!=0 or (self.update!=0 and self.update!=4) or self.agents[offer["agent_id"]["value"]]=="suspended":
						declines.append({"value": offer["id"]["value"]})
					else:
						self.offers[offer["id"]["value"]]={"agent_id": offer["agent_id"]["value"], "resources": {"cpus": res["cpus"], "mem": res["mem"], "disk": 10240}}
				#remove rescinded offers
				for resc in range(len(self.rescinds)-1, -1, -1):
					agent=self.rescinds.pop(resc)
					try:
						self.agents.pop(agent)
						self.sqlite.execute("DELETE FROM agents WHERE agent_id='%s';"%agent)
						self.sqlite_db.commit()
					except:
						if self.debug:
							print("attempted to remove already removed agent", file=sys.stderr)
				#decline offers which don't meet requirements
				if len(declines)>0:
					data={"framework_id": {"value": self.fw_id},
						"type": "DECLINE",
						"decline": {
							"offer_ids": declines,
							"filters": {
								"refuse_seconds": 60
							}
						}
					}
					self.curl.setopt(pycurl.POSTFIELDS, json.dumps(data))
					self.curl.setopt(pycurl.HTTPHEADER, ["Content-Type: application/json", "Mesos-Stream-Id: %s"%self.stream_id])
					self.curl.perform()
					if self.debug:
						print("response code: ", self.curl.getinfo(pycurl.RESPONSE_CODE))


			elif (jdata["type"]=="RESCIND"):
				#offer is gone, remove it and suspend agent
				try:
					agent=self.offers.pop(jdata["rescind"]["offer_id"]["value"])["agent_id"]
					self.agents[agent]="suspended"
					self.rescinds.append(agent)
					self.sqlite.execute("UPDATE agents SET state=1 WHERE agent_id='%s';"%agent)
					self.sqlite_db.commit()
				except:
					if self.debug:
						print("agent is already removed", file=sys.stderr)

			elif (jdata["type"]=="FAILURE"):
				#agent is removed from cluster
				agent=jdata["failure"]["agent_id"]["value"]
				try:
					for offer in self.offers.keys():
						if self.offers[offer]["agent_id"]==agent:
							self.offers.pop(offer)
							break
					if agent in self.agents.keys():
						self.agents.pop(agent)
						self.sqlite.execute("DELETE FROM agents WHERE agent_id='%s';"%agent)
						self.sqlite_db.commit()
					for resc in range(len(self.rescinds)-1, -1, -1):
						if self.rescinds[resc]==agent:
							self.rescinds.pop(resc)
				except:
					if self.debug:
						print("agent is already removed", file=sys.stderr)

			elif (jdata["type"]=="SUBSCRIBED"):
				#first message
				self.fw_id=jdata["subscribed"]["framework_id"]["value"]
				self.sqlite.execute("INSERT INTO state VALUES ('framework', '%s'),('state', 'running'),('sync', 'needed'),('update','needed');"%(self.fw_id))
				self.sqlite_db.commit()

		#check if sync process has finished
		if self.sync != 0:
			pid=os.waitpid(self.sync, os.WNOHANG)
			if os.WIFEXITED(pid[1]) and pid[0] == self.sync:
				if os.WEXITSTATUS(pid[1])==0:
					self.sync=0
					if self.update!=1 and self.update!=4:
						self.update=2
						self.sqlite.execute("UPDATE state SET value='needed' WHERE name='update';")
					self.sqlite.execute("UPDATE state SET value='finished' WHERE name='sync';")
					self.sqlite_db.commit()
					self.list=list(self.db.perform("SELECT pkgname,repo,source,armsource FROM packages WHERE status=2 ORDER BY date;"))
					if self.debug:
						print("sync finished", file=sys.stderr)
				else:
					self.update=3
					self.sqlite.execute("UPDATE state SET value='aborted' WHERE name='sync';")
					self.sqlite_db.commit()
					print("sync failed, aborting", file=sys.stderr)

		#check if update is finished
		if self.update == 1 and self.update_pid != 0:
			pid=os.waitpid(self.update_pid, os.WNOHANG)
			if os.WIFEXITED(pid[1]) and pid[0] == self.update_pid:
				if os.WEXITSTATUS(pid[1])==0:
					self.update=4
					self.update_pid=0
					self.send_mail("update task", "update", "finished")
					self.sqlite.execute("UPDATE state SET value='finished' WHERE name='update';")
					self.sqlite_db.commit()
					if self.debug:
						print("update finished", file=sys.stderr)
				else:
					self.update=3
					self.send_mail("update task", "update", "failed")
					self.sqlite.execute("UPDATE state SET value='aborted' WHERE name='update';")
					self.sqlite_db.commit()
					print("update failed, aborting", file=sys.stderr)

		#if chroot update failed, teardown and exit
		if self.update==3 or fwstate=="stopped":
			data=json.dumps({"framework_id": {"value": self.fw_id}, "type": "TEARDOWN"})
			self.curl.setopt(pycurl.POSTFIELDS, data)
			self.curl.setopt(pycurl.HTTPHEADER, ["Content-Type: application/json", "Mesos-Stream-Id: %s"%self.stream_id])
			self.curl.perform()
			self.sqlite.execute("DROP TABLE agents;")
			self.sqlite.execute("DROP TABLE tasks;")
			self.sqlite.execute("DELETE FROM state;")
			self.sqlite_db.commit()
			if debug:
				print("teardown", file=sys.stderr)
			sys.exit(1)

		#check sqlite state of agents and tasks
		self.sqlite.execute("SELECT task_id,pkgname FROM tasks WHERE state=1;")
		tokill=self.sqlite.fetchall()
		self.sqlite.execute("SELECT agent_id, state FROM agents;")
		agents_list=self.sqlite.fetchall()
		if len(tokill)>0:
			for pkg in tokill:
				task_id=pkg[0]
				data={"framework_id": {"value": self.fw_id},
						"type": "KILL", "kill": {"task_id": {"value": task_id}
						}
					}
				jdata=json.dumps(data)
				self.curl.setopt(pycurl.POSTFIELDS, jdata)
				self.curl.setopt(pycurl.HTTPHEADER, ["Content-Type: application/json", "Mesos-Stream-Id: %s"%self.stream_id])
				self.curl.perform()
				if debug:
					print("killing task %s"%task_id, file=sys.stderr)

		for agent in agents_list:
			if agent[0] in list(self.agents.keys()):
				if agent[1]=="0" or agent[1]==0:
					self.agents[agent[0]]="OK"
				else:
					self.agents[agent[0]]="suspended"
				if debug:
					print("setting agent %s to %s"%(agent[0], self.agents[agent[0]]), file=sys.stderr)
		#check if we should update package list and chroot image: no tasks are running, available packages list is empty, update and sync are not running, framework is not paused
		if (len(self.tasks.keys())==0 and len(self.list)==0 and self.update==0 and self.sync==0 and fwstate!="paused"):
			#TODO: also find a way to run this if there task running and free agents
			self.db.update()
			self.update=2
		#we should get status data from database, check available packages and start them
		if not(self.beats%10):
			#refresh available packages list each 10 HEARTBEATs
			self.list=list(self.db.perform("SELECT pkgname,repo,source,armsource FROM packages WHERE status=2 ORDER BY date;"))
			#for i in self.tasks.keys():
			#	if self.tasks[i]["type"]=="package":
			#		if self.tasks[i]["pkg"] in self.list():
			#			self.list.pop(self.list.index(i))

		if not(self.beats%1000):
			#check for repo sync and start it in seperate process
			if self.sync == 0:
				self.sqlite.execute("UPDATE state SET value='needed' WHERE name='sync';")
				self.sqlite_db.commit()
			self.beats-=1000

		# check if repo sync is needed based on database data
		self.sqlite.execute("SELECT value FROM state WHERE name='sync';")
		sync_state=self.sqlite.fetchone()[0]
		if sync_state == "needed" and fwstate!="paused":
			self.sync = os.spawnlp(os.P_NOWAIT, self.sync_cmd, self.sync_cmd)
			self.sqlite.execute("UPDATE state SET value='running' WHERE name='sync';")
			self.sqlite_db.commit()
			if self.debug:
				print("starting sync process", self.sync, self.sync_cmd, file=sys.stderr)

		# check if chroot update is needed based on database data
		self.sqlite.execute("SELECT value FROM state WHERE name='update';")
		update_state=self.sqlite.fetchone()[0]
		
		if (self.update == 2 or update_state == "needed") and fwstate!="paused":
			#if any packages are being added to databases, wait for processes to finish
			upkgs=0
			for repo in ["system", "world", "galaxy", "asteroids", "armtix"]:
				upkgs+=os.spawnlp(os.P_WAIT, "flock", "flock", "-xn", "/tmp/armtix/%s"%repo, "true")
			if self.debug:
				print("number of packages being added to databases: ", upkgs)

			if upkgs==0:
				self.update = 1
				self.update_pid = os.spawnvp(os.P_NOWAIT, "/usr/bin/chroot-run", ["/usr/bin/chroot-run", "/armtix-master/chroot", "/usr/bin/bash", "-c", "umount /etc/hosts; pacman -Syuu --noconfirm && rm /var/log/pacman.log"])
				self.sqlite.execute("UPDATE state SET value='running' WHERE name='update';")
				self.sqlite_db.commit()
				if self.debug:
					print("starting chroot update", file=sys.stderr)

		if len(self.offers)>0 and len(self.list)>0 and (self.update == 0 or self.update == 4) and self.sync == 0 and fwstate!="paused":
			#starting task
			valid=[x for x in self.offers.keys() if self.agents[self.offers[x]["agent_id"]]=="OK"]
			if len(valid)>0:
				offer_id=random.choice(valid) #choose random offer
				self.start_task(offer_id)

	def start_task(self, offer_id):
		self.update=0
		task_id=''.join(random.choices(string.digits, k=12)) #generate task id
		pkg=self.list.pop(0) #choose package
		url=""
		#generate source file URL and create source archive
		if (pkg[3] != None and pkg[3] != 'None'):#patched package
			path=self.db.perform("SELECT path FROM gitdirs WHERE name='%s';"%pkg[3])[0][0]
			kind=self.db.perform("SELECT kind FROM gitdirs WHERE name='%s';"%pkg[3])[0][0]
			#os.system("rm %s/%s/%s/src.tar.xz 2> /dev/null || /bin/true; cd %s/%s/%s && bsdtar acvf src.tar.xz *"%(path, pkg[1], pkg[0], path, pkg[1], pkg[0]))
			url="%s/%s/%s/%s/"%(self.pdp,pkg[3],pkg[1],pkg[0])
			commit="nocommit"
		else:
			path=self.db.perform("SELECT path FROM gitdirs WHERE name='%s';"%pkg[2])[0][0]
			kind=self.db.perform("SELECT kind FROM gitdirs WHERE name='%s';"%pkg[2])[0][0]
			commit=self.db.perform("SELECT commit FROM packages WHERE pkgname='%s';"%pkg[0])[0][0]
			if pkg[1]=="system":
				repo="core"
			elif pkg[1]=="world":
				repo="extra"
			elif pkg[1]=="galaxy":
				repo="community"
			elif pkg[1]=="asteroids":
				repo="asteroids"
			else:
				repo="unknown"
			#os.system("rm %s/%s/x86_64/%s/src.tar.xz 2> /dev/null || /bin/true; cd %s/%s/x86_64/%s && bsdtar acvf src.tar.xz *"%(path, pkg[0], repo, path, pkg[0], repo))
			url="%s/%s/%s/"%(self.ndp,pkg[2],pkg[0])
		if kind == "gitea":
			checkout = "true"
		else:
			checkout = "false"
		#generate command here
		data={"framework_id": {"value": self.fw_id},
				"type": "ACCEPT", "accept": {
				"offer_ids": [ {"value": offer_id} ],
				"operations": [{ "type": "LAUNCH", "launch": {
					"task_infos": [ {
						"name": pkg[0],
						"task_id": {"value": task_id},
						"agent_id": {"value": self.offers[offer_id]["agent_id"]},
						"command": {#makepkg is ran twice: if some package in cache is broken
							"environment": {"variables": [
								{
									"type": "VALUE",
									"name": "REPO",
									"value": pkg[1]
								}
							]},
							"shell": True,
							"value": """
if [ ! -z $REPO ]; then
	rsync -avz -W -q -x rsync://%s/* ./
	# checkout commit
	%s && git checkout %s
	sudo rsync -avz --delete -W -q -x rsync://%s /armtix-worker/chroot/root
	nproc=$(($(ls /sys/devices/system/cpu|grep cpu[0-9]|wc -l)-2))
	sed -i -e "/build()/,/package*()/s/ninja/ninja -j$nproc /g" -i.bak PKGBUILD
	mkchrootpkg -r /armtix-worker/chroot -c -- -A --nobuild --noprepare --skippgpcheck || mkchrootpkg -r /armtix-worker/chroot -- -A --nobuild --noprepare --skippgpcheck
	mkchrootpkg -r /armtix-worker/chroot -N -- -A --skippgpcheck || mkchrootpkg -r /armtix-worker/chroot -N -- -Ae --skippgpcheck || exit 1
else
	exit 1
fi
echo removing debug packages
rm *-debug-*.pkg.tar.xz
for PKG in *.pkg.tar.xz; do
	ret=1
	while [ $ret != 0 ]; do
		rsync -avz ./$PKG rsync://%s/$REPO/$PKG
		ret=$?
		echo uploading $PKG ended with status $ret
		sleep 1s
	done
done"""%(url, checkout, commit, self.rsync_chroot, self.rsync_upload)
							#"uris": [
							#	{
							#		"executable": False,
							#		"extract": True,
							#		"cache": False,
							#		"value": url
							#	}
							#]
						},
						"resources": [
							{"allocation_info": {"role": self.role}, "name": "cpus", "type" : "SCALAR", "scalar": {"value": self.offers[offer_id]["resources"]["cpus"]}},
							{"allocation_info": {"role": self.role}, "name": "mem", "type": "SCALAR", "scalar": {"value": self.offers[offer_id]["resources"]["mem"]}},
							{"allocation_info": {"role": self.role}, "name": "disk", "type": "SCALAR", "scalar": {"value": self.offers[offer_id]["resources"]["disk"]}}
						]
					} ]
					} }
					]
				}}
		jdata=json.dumps(data)
		self.curl.setopt(pycurl.POSTFIELDS, jdata)
		self.curl.setopt(pycurl.HTTPHEADER, ["Content-Type: application/json", "Mesos-Stream-Id: %s"%self.stream_id])
		self.curl.perform()
		# add task to lists
		self.tasks[task_id]={"type": "package", "pkg": pkg[0], "repo": pkg[1], "state": "TASK_STARTING", "agent_id": self.offers[offer_id]["agent_id"], "resources": [
			{"allocation_info": {"role": self.role}, "name": "cpus", "type" : "SCALAR", "scalar": {"value": self.offers[offer_id]["resources"]["cpus"]}},
			{"allocation_info": {"role": self.role}, "name": "mem", "type": "SCALAR", "scalar": {"value": self.offers[offer_id]["resources"]["mem"]}},
			{"allocation_info": {"role": self.role}, "name": "disk", "type": "SCALAR", "scalar": {"value": self.offers[offer_id]["resources"]["disk"]}}
		]}
		self.sqlite.execute("INSERT INTO tasks VALUES('%s','%s','%s',0);"%(task_id, pkg[0], self.offers[offer_id]["agent_id"]))
		self.sqlite_db.commit()
		self.offers.pop(offer_id)
		self.db.perform("UPDATE packages SET status=3 WHERE pkgname='%s';"%pkg[0])
		if self.debug:
			print("starting package %s"%pkg[0], file=sys.stderr)

	def send_mail(self, task_id, task_type, status, package="", repo=""):
		try:
			now=datetime.datetime.now()
			message='''\
From: %s
To: %s
'''%(self.mu, self.ma)
			if task_type=="update":
				message=message+'''\
Subject: update %s
Date: %s

Update status: %s
Task ID: %s
'''%(status, now.strftime("%a, %d %b %Y %X"), status, task_id)
	
			else:
				message=message+'''\
Subject: %s %s
Date: %s

Package %s build status: %s
Task ID: %s
Repo: %s
'''%(package, status, now.strftime("%a, %d %b %Y %X"), package, status, task_id, repo)
			self.smtp.setopt(pycurl.MAIL_FROM, self.mu)
			self.smtp.setopt(pycurl.MAIL_RCPT, [self.ma])
			self.smtp.setopt(pycurl.UPLOAD, True)
			self.smtp.setopt(pycurl.READDATA, BytesIO(message.encode('ascii')))
			self.smtp.perform()
		except:
			print("failed to send mail", file=sys.stderr)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--rsync_chroot', help="Path to chroot shared via rsync in the form [USER@]HOST[:PORT]/SRC", type=str, required=True)
	parser.add_argument('--rsync_upload', help="rsync path to upload packages in the form [USER@]HOST[:PORT]/SRC", type=str, required=True)
	parser.add_argument('--sqlite', help="Path to sqlite state database", type=str, required=True)
	parser.add_argument('--sync', help="Path to command performing sync (default: ./sync.py)", type=str, default='./sync.py')
	#Network address options
	parser.add_argument('--ndp', help="Normal download path in the form [USER@]HOST[:PORT]/SRC. This determines where are normal PKGBUILD archives downloaded from", type=str, required=True)
	parser.add_argument('--pdp', help="Patched download path in the form [USER@]HOST[:PORT]/SRC. This determines where are patched PKGBUILD archives downloaded from", type=str, required=True)
	parser.add_argument('--mh', help='SMTP mail host to send notifications to', type=str, required=True)
	parser.add_argument('--ma', help='Address to send mail to (including domain name)', type=str, required=True)
	parser.add_argument('--mu', help='User to send mail from (including domain name); authentication is not implented now', type=str, required=True)
	#Mesos connection options
	parser.add_argument('--url', help = 'Mesos master URL (host:port)',type=str,required=True)
	parser.add_argument('--role', help = 'Mesos role name',type=str,required=True)
	parser.add_argument('--name', help = 'Framework name (default: ARMtix builder)',type=str,default='Armtix builder')
	#Database connection options
	parser.add_argument('--database', help = 'Database name',type=str,required=True)
	parser.add_argument('--host', help = 'Database host address',type=str)
	parser.add_argument('--port', help = 'Database port (default: 3306)',type=int,default=3306)
	parser.add_argument('--unix', help = 'Unix socket for database',type=str)
	parser.add_argument('--user', help = 'Database user to connect to',type=str,required=True)
	parser.add_argument('--password', help = 'Users password if needed',type=str,default='')
	parser.add_argument('--compress', help = 'Set True to avail compression')
	#misc
	parser.add_argument('--debug', help = 'Enable debug messages')
	args = parser.parse_args()

	#sqlite database
	if (os.path.isfile(args.sqlite)):
		#check if other framework is running, abort if it is
		conn=sqlite3.connect(args.sqlite)
		cur=sqlite3.Cursor(conn)
		try:
			cur.execute("SELECT value FROM state WHERE name='framework';")
			fw=cur.fetchall()
			if len(fw)>0:
				print("another framework is running", file=sys.stderr)
				sys.exit(1)
			try:
				cur.execute("CREATE TABLE tasks(task_id TEXT, pkgname TEXT, agent_id TEXT, state INT, PRIMARY KEY(task_id));")
				cur.execute("CREATE TABLE agents(agent_id TEXT, host TEXT, state INT, PRIMARY KEY(agent_id));")
				conn.commit()
			except:
				cur.execute("DROP TABLE tasks;")
				cur.execute("DROP TABLE agents;")
				cur.execute("CREATE TABLE tasks(task_id TEXT, pkgname TEXT, agent_id TEXT, state INT, PRIMARY KEY(task_id));")
				cur.execute("CREATE TABLE agents(agent_id TEXT, host TEXT, state INT, PRIMARY KEY(agent_id));")
				conn.commit()
		except:
			print("%s is not sqlite database, aborting"%args.sqlite, file=sys.stderr)
			sys.exit(1)
		cur.close()
		conn.close()
	else:
		#create database
		os.mkdir(os.path.dirname(args.sqlite), mode=0o770)
		shutil.chown(os.path.dirname(args.sqlite), group="http")
		os.mknod(args.sqlite, mode=0o660)
		shutil.chown(args.sqlite, group="http")
		conn=sqlite3.connect(args.sqlite)
		cur=sqlite3.Cursor(conn)
		cur.execute("CREATE TABLE state(name TEXT, value TEXT, PRIMARY KEY(name));")
		cur.execute("CREATE TABLE tasks(task_id TEXT, pkgname TEXT, agent_id TEXT, state INT, PRIMARY KEY(task_id));")
		cur.execute("CREATE TABLE agents(agent_id TEXT, host TEXT, state INT, PRIMARY KEY(agent_id));")
		conn.commit()
		cur.close()
		conn.close()

	#Database
	compress=False
	debug=False
	if args.compress:
		compress=True
	if args.debug:
		debug=True
	operation=()
	if args.unix:
		operation=workclass(args.role, args.sqlite, args.sync, args.url, args.rsync_chroot, args.rsync_upload, args.ndp, args.pdp, args.mh, args.ma, args.mu, args.user,args.password,args.database,db_unix=args.unix,db_compress=compress,debug=debug)
	elif args.host:
		operation=workclass(args.role, args.sqlite, args.sync, args.url, args.rsync_chroot, args.rsync_upload, args.ndp, args.pdp, args.mh, args.ma, args.mu, args.user,args.password,args.database,db_host=args.host,db_port=args.port,db_compress=compress,debug=debug)
	else:
		print("Either database host or UNIX socket must be specified!", file=sys.stderr)
		sys.exit(1)

	#Connection
	handle=pycurl.Curl()
	data=json.dumps({
		"type": "SUBSCRIBE",
		"subscribe": {
			"framework_info": {
				"capabilities": {"type": "MULTI_ROLE" },
				"user": args.role, 
				"name": args.name,
				"roles": args.role
			}
		}
	})
	handle.setopt(handle.URL, "http://%s/api/v1/scheduler"%args.url)
	handle.setopt(handle.POSTFIELDS, data)
	handle.setopt(handle.HTTPHEADER, ["Content-Type: application/json", "Connection: Keep-Alive", "Accept: application/json"])
	handle.setopt(handle.HEADERFUNCTION, operation.headerfunction)
	handle.setopt(handle.WRITEFUNCTION, operation.writefunction)
	handle.perform()
	handle.close()
