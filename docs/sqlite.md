SQLite runtime database
-----------------------
This database is used to store runtime data of running framework: its state, tasks and known agents

Tables:

* state - contains framework id, its state and state of chroot update and database sync tasks
* tasks - lists running tasks
* agents - lists known agents

Table *state* stores data in format name|value. It contains the following fields:

* framework - stores framework ID
* state - stores framework states
* sync - stores database sync state
* update - stores chroot update state

Possible framework states: running, paused, stopped. By default framework is *running* which means that it operates normally. If framework state is set to
*paused*, framework will not accept new tasks: it will neither start new builds, nor sync database, nor update chroot. However, it will still react to incoming
Mesos messages. This means that it can set update/sync to *needed* but will not run them until it is unpaused. If state is *stopped* framework will stop once it gets
new Mesos message. This is the safe method to stop framework.

Possible DB sync and chroot update states: needed/running/finished/aborted. At start they are both set to needed and will be start with first Mesos message.
If corresponding process exits normally, its state is set to finished, otherwise it is aborted and framework will exit. By default update is set to *needed*
after sync is finished but if there were no builds after last update no new update is performed.

Table *tasks* stores running tasks info in format task\_id|pkgname|agent\_id|state. By default state is 0, if it is set to 1, task will be killed and marked as failed.

Table agents stores known agents in format agent\_id|host|state. The default state 0 means that everything is OK. If it is set to 1, this agent will not receive
new tasks.

sqlite.tcl script provides HTTP CGI interface for interacting with SQLite database over network. Since it is executed without arguments it tries to find database based on
data in */etc/fw-sqlite/path*. This file must only contain path to database. Database itself and directory it is located in must have correct rw
permissions to be accessible from HTTP server.
