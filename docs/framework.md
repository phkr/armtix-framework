Framework
---------
framework.py is the main executable. It performs all the logic and interaction with Mesos.

Arguments:

* rsync\_chroot - path in the form [USER@]HOST[:PORT]/SRC used by agents to download chroot via rsync
* rsync\_upload - path in the form [USER@]HOST[:PORT]/SRC used by agents to upload packages via rsync
* sqlite - path runtime SQLite database
* sync - path to command performing database sync (see below)
* ndp - normal download path (see below)
* pdp - patched download path (see below)
* mh - SMTP server host
* ma - SMTP receiver address
* mu - SMTP sender address
* url - Mesos master URL
* role - Mesos role name (workers running as root will use this as user name mkchrootpkg is run as)
* name - framework name
* database, host, port, unix, user, password, compress - see gittodb
* debug - enable debug messages

At start framework checks if SQLite database is present. If it is owned by other framework (it has some tables) then framework is not started.
If there is no database, it is created and its group ownership is set to http.

Sync
====
sync.py pulls gitdirs and run sync() and update() methods. However, to run successfully you need to specify gittodb connection options.

Download paths
==============
Mainline Artix and patched ARMtix repos have different structure. In both cases agents retrieve source files via rsync. Both ndp and pdp have form [USER@]HOST[:PORT]/SRC.
Artix repos are accessed as $npd/$gitdir/$pkgname/x86\_64/$repo/. ARMtix repos are accesses as $pdp/$gitdir/$repo/$pkg/.
