Prerequisites
-------------

Software
========
You need to have the following software installed on master and worker nodes:
* [Mesos](https://mesos.apache.org/) - main software used
* rsync - to share files between
* [wendy](http://z3bra.org/wendy) - used on master node to add new packages to repos
* TCL and Python - to execute scripts on master node
* MySQL - to store git repos states on master
* python-mysqlclient and python-pygit2 packages
* SQLite + TCL package - for runtime SQLite database
* SMTP server - for sending notifications
* artools - provides chroot-run and mkchrootpkg

Filesystem
==========
Master node must have package upload directory, git repos and base chroot directories accessible via rsync.
Since rsync handles the paths itself you may choose them arbitrary.

Worker nodes must have directory */armtix-worker/chroot* which is used to keep build chroot. Although it depends
on init scripts used, */armtix-worker/mesos-worker* directory is also expected to be present.

Configuration
=============
Framework option *role* defines Mesos cluster role name which works as user name task is run as when mesos-worker
runs as root. This user must exist on each worker node and passwordless access to the following commands:
* *rsync* - to update chroot copy
* *mkchrootpkg* - to run them without password

Master node has to update chroot periodically. To do this a user running framework must have passwordless access
to *chroot-run* command.
