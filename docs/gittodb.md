GitToDB
-------
File gittodb.py provides module for interaction between MySQL database and running framework.

Support for other databases can be added in future.

Database schema
===============
Tables:

* nobuild - list of packages which should not be built, this table only contains names
* gitdirs - list of directories with git repositories
* chains - list of packages which should be built manually in particular order
* packages - list of packages with all their data

Gitdirs table fields:

* name - repo name used to identify it in framework; it is also used by rsync to get source (see framework documentation)
* path - location of repo directory
* commit - latest commit in this repo
* arm - a string value used to identify if package needs patch before build

Primary key is (name,path)

Chains table fields:

* pkgname - package name
* chain - chain name this package belongs to
* source - Artix repo it belongs to (by name field of gitdirs)
* commit - latest commit for package
* armsource - repo with patched PKGBUILDs for this package
* arm - latest commit for patched source
* status - build status of package

Primary key is (pkgname, chain)

Packages table fields:

* pkgname - package name based on pkgbase field of PKGBUILD
* pkgver - version of package
* provide - list of provided packages based on all pkgname and provides fields of PKGBUILD
* deps - list of dependencies based depends fields of PKGBUILD (without versions)
* makedepends - list of make dependencies based on makedepend field of PKGBUILD (without versions)
* repo - which repo this package belongs to
* source - Artix repo it belongs to (by name field of gitdirs)
* commit - latest commit for package
* armsource - repo with patched PKGBUILDs for this package
* arm - latest commit for patched source
* date - date of latest commit in UNIX format
* status - build status of package

Primary key is pkgname

Build statuses:

* 0 - successful build
* 1 - build failed
* 2 - package can be built
* 3 - building in process
* 4 - package waits for dependency
* 5 - package waits for patch
* 6 - package waits for mainline commit (e.g. patch is already present but mainline commit is needed for correct ordering)
* 7 - package can be built but is part of chain
* 8 - processing: all needed updates are present, package must be checked for dependencies

Usage
=====
Most of relevant usage is already included in framework.py and sync.py. You only need to manually add git repos to database.
In order to achieve this you need to follow the next scheme:

* create GitToDB instance. It requires database user name, password ("" if emtpy), database name and connection details. The latter is either path to UNIX socket or
host address (with port if needed).

* call add\_gitdirs method. It requires a list of git repos paths and their reference names in form \[("path1", "name1"), ("path2", "name2")]. If repos in this list correspond to patched packages, the second argument also has to be provided (e.g. arm="aarch64")

* call add\_nobuild method to add a list of packages which are not processed by framework in any way

* call add\_chains method to add chains of packages (see chains table). The argument is a dictionary in form "chain\_name": ["pkgname1", "pkgname2"]

If sync.py script is not enough for you (example: you want to update database with local copy of gitdirs without pulling them) you need only sync and update methods.
