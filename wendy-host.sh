#!/bin/bash
export REPO=$(dirname "$WENDY_INODE")
export PKG=$(basename "$WENDY_INODE")

mv "$WENDY_INODE" "../repo/$REPO/os/aarch64/$PKG"
cd ../repo/$REPO/os/aarch64
flock -x /tmp/armtix/$REPO bash -c '
echo $REPO $PKG
ret=1
while [[ $ret != 0 ]]; do
	repo-add -R $REPO.db.tar.xz $PKG
	ret=$?
	sleep 1s
done
'
