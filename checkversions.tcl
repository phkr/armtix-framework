#!/usr/bin/tclsh

# script reads 2 files containing lists of packages in repos in format: name [ one space ] version
# the list may be created by command: pacman -Sl <repo> | awk '{print $2,$3}'

set repo1 [lindex $argv 0]
set repo2 [lindex $argv 1]

set file1 [open [string cat $repo1 .files] r]
set read1 [read -nonewline $file1]
close $file1
set packages1 [eval dict create [split $read1]]

set file2 [open [string cat $repo2 .files] r]
set read2 [read -nonewline $file2]
close $file2
set packages2 [eval dict create [split $read2]]

foreach {pkg} [dict keys $packages1] {
  if [dict exists $packages2 $pkg] {
    set version [split [dict get $packages1 $pkg] -]
    set ver1 [lindex $version 0]
    set rel1 [lindex $version 1]
    set version [split [dict get $packages2 $pkg] -]
    set ver2 [lindex $version 0]
    set rel2 [lindex $version 1]

    if [string equal $ver1 $ver2] {
      if [string equal $rel1 $rel2] {
        puts "samerel: $pkg $ver1-$rel1 in both $repo1 and $repo2"
      } else {
        puts "samever: $pkg $ver1-$rel1 in $repo1, $ver2-$rel2 in $repo2"
      }
    } else {
      puts "newver: $pkg $ver1-$rel1 in $repo1, $ver2-$rel2 in $repo2"
    }
  }
}
