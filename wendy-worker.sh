#!/bin/bash
REPO=$(dirname "$WENDY_INODE")
PKG=$(basename "$WENDY_INODE")

ret=1
while [[ $ret != 0 ]]; do
	rsync -avz "$WENDY_INODE" rsync://armtix@armtix-master.example.com/armtix/$REPO/$PKG
	ret=$?
	sleep 1s
done

rm "$WENDY_INODE"
