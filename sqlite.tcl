#!/bin/tclsh
#Task: provide HTTP interface for SQLite database
package require sqlite3

#from https://rosettacode.org/wiki/URL_decoding#Tcl
proc urlDecode {str} {
	set str [string map {+ " "} $str]
	set specialMap {"[" "%5B" "]" "%5D"}
	set seqRE {%([0-9a-fA-F]{2})}
	set replacement {[format "%c" [scan "\1" "%2x"]]}
	set modStr [regsub -all $seqRE [string map $specialMap $str] $replacement]
	return [encoding convertfrom utf-8 [subst -nobackslash -novariable $modStr]]
}
#read config
set config_file /etc/fw-sqlite/path
set config_fd [open $config_file r]
set sqlite_path [gets $config_fd]
close $config_fd
unset config_file
#setting input data
set inp {}
while {[gets stdin tmp]>0} {set inp [format {%s%s} $inp $tmp]}

set encdata [split $inp &]
set decdata {}
foreach {field} $encdata {
	set decdata [format {%s %s} $decdata [split $field =]]
}

#now it is a list of keys and values but in encoded form
set decdata [concat $decdata]
set i 0
while {$i < [llength $decdata]} {
	lset decdata $i [urlDecode [lindex $decdata $i]]
	incr i
}
unset i

array set commands $decdata
#perform operation if needed
set reply {}
set errorMsg {}
if {[lsearch -exact [array names commands] command] > -1} {
	set command $commands(command)

	catch {
		sqlite3 sqlite_db $sqlite_path
		sqlite_db eval $command result {
			if {[llength [array names result]] > 1} {
				#set names [lsort -ascii [lrange [array names result] 0 end-1]]
				set star [lsearch -exact [array names result] *]
				set names [lsort -ascii [lreplace [array names result] $star $star]]
				set reply [format {%s<tr>} $reply]
				foreach {name} $names {
					set reply [format {%s<td>%s</td>} $reply $result($name)]
				}
				set reply [format {%s</tr>} $reply]

			}
		}
		if {[string length $reply]>0} {
			set header {<table><caption>result:</caption><tr>}
			foreach {name} $names {set header [format {%s<th>%s</th>} $header $name]}
			set reply [format {%s</tr>%s</table>} $header $reply]
		}
		sqlite_db close
	} errorMsg
}

if {[string length $errorMsg] != 0 } {
	set reply "
<h2>Error while processing script:</h2>
<p>$errorMsg</p>
"}

if {[string length $reply] == 0} {
	set reply "
<h1>Database info</h1>
<p>To get SQLite database metadata run SELECT on sqlite_master table</p>
<h2>Table state</h2>
<p>Table has format name|value represented by strings</p>
<p>Possible names:</p>
<ul>
<li>framework: represents framework id</li>
<li>state: represents one of framework states - running, paused or stopped</li>
<li>sync: represents repo-to-db sync state - one of needed, running, finished, aborted</li>
<li>update: represents chroot update state - one of needed, running, finished, aborted</li>
</ul>
<p>If framework is paused, sync or update is running, all new offers are declined</p>
<h2>Table tasks</h2>
<p>Table has format task_id|pkgname|agent_id|state</p>
<p>State 0 corresponds to normally running task, state 1 means that task must be killed</p>
<h2>Table agents</h2>
<p>Table has format agent_id|host|state</p>
<p>State 0 corresponds to normally running agent, state 1 corresponds to suspended agent (doesn't accept new builds, offers from this agent are declined)</p>
"}

puts stdout [format {Content-type: text/html; charset=utf-8

<head>
<link rel="stylesheet" href="styles.css">
</head>
<body>
<p>This page allows you to run SQL queries on ARMtix SQLite runtime database</p>
<form action="%s" method="post">
<fieldset><legend>Send sqlite query</p>
<input type="text" name="command" size=100>
<input type="submit" value="Send"></fieldset>
</form>

<table><caption>Useful queries:</caption>
<tr>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="SELECT * FROM state">
<input type="submit" value="Get framework state"></fieldset>
</form></td>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="SELECT * FROM tasks">
<input type="submit" value="Get tasks"></fieldset>
</form></td>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="SELECT * FROM agents">
<input type="submit" value="Get agents state"></fieldset>
</form></td>
</tr><tr>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="UPDATE state SET value='paused' WHERE name='state'">
<input type="submit" value="Pause framework"></fieldset>
</form></td>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="UPDATE state SET value='running' WHERE name='state'">
<input type="submit" value="Unpause framework"></fieldset>
</form></td>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="UPDATE state SET value='stopped' WHERE name='state'">
<input type="submit" value="Teardown framework"></fieldset>
</form></td>
</tr><tr>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="UPDATE state SET value='needed' WHERE name='update'">
<input type="submit" value="Update chroot"></fieldset>
</form></td>
<td><form action="%s" method="post">
<fieldset>
<input type="hidden" name="command" value="UPDATE state SET value='needed' WHERE name='sync'">
<input type="submit" value="Sync database"></fieldset>
</form></td>
</tr>
</table>

%s
</body>
} $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $env(SCRIPT_NAME) $reply]
